@interface EXTERNAL
@property(assign) BOOL shouldCacheResponseData;
+(id)gDataService;
+(NSString*)serviceRootURLString;
-(void)clearLastModifiedDates;
-(id)fetchEntryWithURL:(NSURL*)URL completionHandler:(id)handler;
-(UITabBarController*)mainTabBarController;
-(void)setAutoPlay:(BOOL)play;
-(void)setVideo:(id)entry;
@end

static NSURL* $_externalURL=nil;
static void $_loadURL(NSURL* URL,UITabBarController* tabbar){
  tabbar.selectedIndex=0;
  UIViewController* home=tabbar.selectedViewController;
  UITabBarItem* item=home.tabBarItem;
  if(item.badgeValue){return;}
  item.badgeValue=@"!";
  NSString* URLString=[[%c(GDataServiceGoogleYouTube) serviceRootURLString]
   stringByAppendingFormat:@"api/videos/%@?v=2",URL.resourceSpecifier];
  id service=[%c(YCYoutubeService) gDataService];
  [service setShouldCacheResponseData:YES];
  [service fetchEntryWithURL:[NSURL URLWithString:URLString]
   completionHandler:^(id ticket,id entry,NSError *error){
    item.badgeValue=nil;
    if(error){
      UIAlertView* alert=[[UIAlertView alloc]
       initWithTitle:error.localizedDescription message:URLString delegate:nil
       cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel",nil,
        [NSBundle bundleWithIdentifier:@"com.apple.UIKit"],@"Cancel")
       otherButtonTitles:nil];
      [alert show];
      [alert release];
      return;
    }
    id controller=[home.storyboard
     instantiateViewControllerWithIdentifier:@"YAVideoDetailViewController"];
    [controller setVideo:entry];
    [controller setAutoPlay:YES];
    [(UINavigationController*)home pushViewController:controller animated:YES];
  }];
}

%hook YAAppDelegate
%new
-(BOOL)application:(UIApplication*)application openURL:(NSURL*)URL sourceApplication:(NSString*)identifier annotation:(id)annotation {
  if(![URL.scheme isEqualToString:@"youtube"]){return NO;}
  UITabBarController* tabbar=[self mainTabBarController];
  if(tabbar.isViewLoaded){$_loadURL(URL,tabbar);}
  else {
    if($_externalURL){[$_externalURL release];}
    $_externalURL=[URL retain];
  }
  return YES;
}
%end

%hook YATabBarController
-(void)viewDidAppear:(BOOL)animated {
  %orig;
  if($_externalURL){
    $_loadURL($_externalURL,(id)self);
    [$_externalURL release];
    $_externalURL=nil;
  }
}
%end
